﻿
Partial Class FlightInt_BookingMsg
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim msg As String = ""
        msg = Request.QueryString("msg")
        If msg = "CL" Then
            lblMsg.Text = "Insufficient Credit Limit. Please contact administrator."
        ElseIf msg = "NA" Then
            lblMsg.Text = "You are not authorised for booking. Please contact administrator."
        ElseIf msg = "PRC" Then
            lblMsg.Text = "System is unable to price this itinerary, Please select another."
        ElseIf msg = "ML" Then
            lblMsg.Text = "The Meal or Baggage you selected is not avialable, Please try another search."
            'ElseIf msg = "SSR" Then
            '    lblMsg.Text = "The Baggage you selected is not avialable, Please try another search."
        ElseIf msg = "1" Then
            lblMsg.Text = "Please make another search for new booking."
        ElseIf msg = "2" Then
            lblMsg.Text = "Corrently our database is too busy, Please try after sometime."
        End If
    End Sub
End Class
