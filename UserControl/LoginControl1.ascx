﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LoginControl1.ascx.cs" Inherits="UserControl_LoginControl1" %>

<link href='<%= ResolveClientUrl("~/newcss/style.css")  %>' rel="stylesheet" />

<hgroup>
    <div class="row">
          <div class="large-14 medium-12 text-center ">
                <%--<img src="<%= ResolveUrl("../images/logo.png") %>" alt="ITZ Logo" border="0" />--%>
              <img src="<%= ResolveUrl("../images/logo-new.png") %>" alt="Logo" border="0" style="width:100%"/>
            </div>
            
        </div>
        <div class="clear"></div>
    <h2>Login Here</h2>
</hgroup>
<form>
    <div class="group">
        <asp:TextBox ID="UserName" runat="server"></asp:TextBox><span class="highlight"></span><span class="bar"></span>
        <label>User Id</label>
    </div>
    <div class="group">
        <asp:TextBox ID="Password" runat="server" TextMode="Password"></asp:TextBox><span class="highlight"></span><span class="bar"></span>
        <label>Password</label>
    </div>
     <asp:Button ID="LoginButton" runat="server"  OnClick="LoginButton_Click" CssClass="button buttonBlue"
                    Text="Login Here" />
  
    <asp:Label ID="LblIPAddress" runat="server" style="display:none;" />
     <asp:Label ID="HTTP_X_FORWARDED_FOR" runat="server" style="display:none;" />
     <asp:Label ID="REMOTE_ADDR" runat="server" style="display:none;" />
    <asp:Label ID="UserLocalHost" runat="server" style="display:none;" />
</form>

<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src="js/index.js"></script>
<%--<asp:Login ID="UserLogin" runat="server">--%>
<%-- <TextBoxStyle />
    <LoginButtonStyle />
    <LayoutTemplate>
        <div class="large-12 medium-12 small-12">
            <div class="lft f16" style="display:none;">
                Login Here As <asp:DropDownList ID="ddlLogType" runat="server">
                    <asp:ListItem Value="C">Customer</asp:ListItem>
                    <asp:ListItem Value="M">Management</asp:ListItem>
                              </asp:DropDownList>
            </div>

            <hgroup>
        
        <h2>Login Here</h2>
    </hgroup>

    <form>
        <div class="group">
            <asp:TextBox ID="UserName" runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                    ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="UserLogin">*</asp:RequiredFieldValidator><span class="highlight"></span><span class="bar"></span>
            <label>User ID</label>
        </div>
        <div class="group">
            <asp:TextBox ID="Password" runat="server" TextMode="Password"></asp:TextBox>
            <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                    ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="UserLogin">*</asp:RequiredFieldValidator><span class="highlight"></span><span class="bar"></span>
            <label>Password</label>
        </div>
        <asp:Button ID="LoginButton" runat="server" ValidationGroup="UserLogin" OnClick="LoginButton_Click" CssClass="btn"
                    Text="Log In" />
                                        Login Here
            <div class="ripples buttonRipples"><span class="ripplesCircle" style="top: 35.2031px; left: 147px;"></span></div>
                                    </button>
    </form>
  
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="js/index.js"></script>--%>
<%--<div class="rgt">
                <a href="ForgotPassword.aspx" rel="lyteframe" class="forgot">Forgot Your password (?)</a>
            </div>--%>

<%-- <div class="clear1">
            </div>
            
            <div class="large-3 medium-3 small-11 columns">
              
               UserId:
                <asp:TextBox ID="UserName" runat="server"></asp:TextBox>
               
            </div>
            <div class="large-1 medium-1 small-1 columns">
                 <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                    ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="UserLogin">*</asp:RequiredFieldValidator>
            </div>
            
            <div class="large-3 medium-3 small-11 columns">
               
              Password:
                  <asp:TextBox ID="Password" runat="server" TextMode="Password"></asp:TextBox>
                
            </div>
            <div class="large-1 medium-1 small-1 columns"><asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                    ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="UserLogin">*</asp:RequiredFieldValidator></div>
            
            <div class="large-4 medium-4 small-12 columns">
                <div class="clear1">
            </div>
                <asp:Button ID="LoginButton" runat="server" ValidationGroup="UserLogin" OnClick="LoginButton_Click" CssClass="btn"
                    Text="Log In" />
            </div>
            <div class="clear">
            </div>
            <div>
                <asp:Label ID="lblerror" Font-Size="10px" runat="server" ForeColor="Red"></asp:Label>
            </div>
            <div class="clear">
            </div>--%>
<%--</div>
    </LayoutTemplate>
    <InstructionTextStyle Font-Italic="True" ForeColor="Black" />
    <TitleTextStyle />
</asp:Login>--%>

