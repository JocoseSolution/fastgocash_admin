﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="FltSearch.ascx.vb" Inherits="UserControl_FltSearch" %>
<link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
    rel="stylesheet" />


<div class="w100 serchbox mtop20">
    
        
        <div class="row">
            <div class="tital large-12 medium-12 small-12">Search Domestic And International Flights</div>
             <div class="titalr large-12 medium-12 small-12">Search Flights</div>
            <div class="clear"></div>
            <div class="large-12 medium-12 small-12">
            <input type="radio" name="TripType" value="rdbOneWay" id="rdbOneWay" checked="checked" />
            One-Way
            <input type="radio" name="TripType" value="rdbRoundTrip" id="rdbRoundTrip" /> 
            Round-Trip </div>
        </div>
        
                     <div class="clear1"></div> 
    <div class="row">
        <div class="large-12 medium-12 small-12 columns">
    <div class="large-5 medium-5 small-12 columns">       
                                <div class="lft sec1">
                                    <input type="text" name="txtDepCity1" value="From" onClick="this.value = '';" id="txtDepCity1"/>
                                    <input type="hidden" id="hidtxtDepCity1" name="hidtxtDepCity1" value="" />
                                </div> </div>  

    <div class="large-5 medium-5 small-12 columns">
                           
                                <div class="sec1">
                                    <input type="text" name="txtArrCity1" value="To" onClick="this.value = '';" id="txtArrCity1"/>
                                    <input type="hidden" id="hidtxtArrCity1" name="hidtxtArrCity1" value="" />
                                </div></div>
        </div> </div>
        <div class="row">
<div class="large-12  medium-12 small-12 columns">

                           
    <div class="large-5 medium-5 small-12 columns">
    <div class="sec1">
                                <div class="w49 lft">Depart :<br />
                                
                                
                                    <input type="text" name="txtDepDate" id="txtDepDate" class="txtCalander" value=""
                                        readonly="readonly" />
                                    <input type="hidden" name="hidtxtDepDate" id="hidtxtDepDate" value="" />
                               </div>
                           
                                <div class="w49 rgt" id="trRetDateRow">Return :<br />
                               
                               
                                    <input type="text" name="txtRetDate" id="txtRetDate" class="txtCalander" value=""
                                        readonly="readonly" />
                                    <input type="hidden" name="hidtxtRetDate" id="hidtxtRetDate" value="" />
                                
                                </div> </div> </div>

    <div class="large-5 medium-5 small-12 columns">
    <div class="sec1 lft">
                           
           
       <div class="large-2 medium-4 small-4 columns"> <div class="w100"><strong> Adult (12+)<br /></strong></div>
                                <div class="mright10 lft">
                                    <img src="<%=ResolveUrl("../Images/adult.png") %>" />
                                </div>
                                <div class="mright10 lft"> 
                                    <select name="Adult" id="Adult">
                                        <option value="1" selected="selected">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                    </select>
                                </div></div>
                                
                 <div class="large-2 medium-4 small-4 columns">
                     <div class="w100"><strong> Child (02-12)<br /></strong></div>  
                     <div class="lft mright10"> <img src="<%=ResolveUrl("../Images/child.png")%>" />
                                </div>
                                <div class="lft mright10">
                                    <select name="Child" id="Child">
                                        <option value="0">0</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                    </select>
                                </div>
                 </div>          
                            
                           <div class="large-2 medium-4 small-4 columns end">
                     <div class="w100"><strong> Infant (0-2)<br /></strong></div>      
                           
                                <div class="lft mright10"> <img src="<%=ResolveUrl("../Images/infant.png")%>" />
                                </div>
                                <div class="lft mright10">
                                    <select name="Infant" id="Infant">
                                        <option value="0">0</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                    </select>
                                </div> 

                               </div>
                           </div>  </div> 


        <%--<div class="sec1 lft wht black passenger">
                           
           
        <div class="w100"><strong>  Choose Passengers</strong><br />
                                </div>
                           
                                <div class="lft">Adults(12+)
                                </div>
                                <div class="mright10 lft">
                                    <select name="Adult" id="Adult">
                                        <option value="1" selected="selected">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                    </select>
                                </div>
                            
                                <div class="lft">Child(2-11)
                                </div>
                                <div class="lft mright10">
                                    <select name="Child" id="Child">
                                        <option value="0">0</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                    </select>
                                </div>
                           
                                <div class="lft">Infants
                                </div>
                                <div class="lft mright10">
                                    <select name="Infant" id="Infant">
                                        <option value="0">0</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                    </select>
                                </div></div>--%>
    </div>
                        
         </div>

    <div class="row">
        <div class="large-12 medium-12 small-12 columns">
    
    
    <div class="large-5 medium-5 small-12 columns">
                                <div class="large-4 medium-4 small-12 columns">Prefer Non-Stop Flight
                                
                                <span class="lft mright10">
                                    <input type="checkbox" name="chkNonstop" id="chkNonstop" value='Y' />
                                </span></div>
                           
                            <div class="lft" id="trAdvSearchRow" style="display: none">
                                <div class="lft ptop10">All Fare Classes
                                </div>
                                <div class="lft mright10">
                                    <input type="checkbox" name="chkAdvSearch" id="chkAdvSearch" value="True" />
                                </div>
                            </div>
                           
                                <div  class="large-4 medium-4 small-12 columns">Gds Round Trip Fares
                                
                                <span class="lft mright10">
                                    <input type="checkbox" name="GDS_RTF" id="GDS_RTF" value="True" />
                                </span></div>
                          
                                <div  class="large-4 medium-4 small-12 columns">Lcc Round Trip Fares
                                
                                <span class="lft mright10">
                                    <input type="checkbox" name="LCC_RTF" id="LCC_RTF" value="True" />
                                </span></div>
                           
                               
           
               
</div>
    

    <div class="large-5 medium-5 small-12 columns">
        <div class="sec1">
<div class="lft w49">Class :<br />
    <select name="Cabin" id="Cabin" class="">
                                        <option value="" selected="selected">--All--</option>
                                        <option value="C">Business</option>
                                        <option value="Y">Economy</option>
                                        <option value="F">First</option>
                                        <option value="W">Premium Economy</option>
                                    </select>
                                </div>

            <div class="rgt w49">Airline :<br />
                 <input type="text" name="txtAirline" value="" id="txtAirline"/>
                                    <input type="hidden" id="hidtxtAirline" name="hidtxtAirline" value="" />
        </div>

         
                     </div>    </div>        
        
        </div>
        </div>
                         
                 <div class="clear"></div>   

    

    
    <div class="w48 rgt">
        <div class="large-6 medium-6 small-12 large-push-6 medium-push-6 columns">
                        <input type="button" id="btnSearch" name="btnSearch" value="Search"/>
                    </div></div>
    <div class="clear"></div>
</div>
<script type="text/javascript">
    var myDate = new Date();
    var currDate = (myDate.getDate()) + '/' + (myDate.getMonth() + 1) + '/' + myDate.getFullYear();
    document.getElementById("txtDepDate").value = currDate;
    document.getElementById("hidtxtDepDate").value = currDate;
    document.getElementById("txtRetDate").value = currDate;
    document.getElementById("hidtxtRetDate").value = currDate;
    var UrlBase = '<%=ResolveUrl("~/") %>';
</script>

<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/change.min.js") %>"></script>
<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/search3.js") %>"></script>

