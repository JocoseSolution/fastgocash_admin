﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="BusServiceCharge.aspx.cs" Inherits="BS_BusServiceCharge" %>

<%@ Register Src="~/UserControl/AccountsControl.ascx" TagPrefix="uc1" TagName="Account" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />

    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>

    <script type="text/javascript">
        function phone_vali() {
            if ((event.keyCode > 47 && event.keyCode < 58) || (event.keyCode == 32) || (event.keyCode == 45))
                event.returnValue = true;
            else
                event.returnValue = false;
        }
    </script>

    <%--<div style="width: 45%; margin: auto; padding: 10px; border: 1px solid #ccc; text-align: left;">
       
        <div style="line-height: 30px; font-weight: bold; font-size: 16px; color: #888; padding-left: 20px; border-bottom: 2px solid #ccc;">
            Bus Cancellation Charges
        </div>

        <table style="width: 100%;" cellspacing="10">
            <tr>

                <td class="fontStyle">Cancellation Charge
                </td>
                <td class="fontStyle">Type
                </td>
                 <td class="fontStyle">Provider Name
                </td>
            </tr>
            <tr>
                <td>

                    <asp:TextBox runat="server" ID="mk"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RFVMK" runat="server" ControlToValidate="mk" ErrorMessage="*"
                        Display="dynamic"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                </td>
                <td>
                    <asp:DropDownList ID="ddl_mktyp" runat="server">
                        <asp:ListItem Value="F">Fixed (F)</asp:ListItem>
                        <asp:ListItem Value="P">Percentage (P)</asp:ListItem>
                    </asp:DropDownList>
                </td>

                 <td>
                    <asp:DropDownList ID="ddl_providername" runat="server">
                        <asp:ListItem Value="RB">RedBus</asp:ListItem>
                        <asp:ListItem Value="GS">GujratService</asp:ListItem>
                    </asp:DropDownList>
                </td>


            </tr>
            <tr>

                <td>
                    <asp:Button ID="Button1" runat="server" OnClick="btnAdd_Click" Text="Add New" />
                </td>

            </tr>

            <tr>
                <td>
                    <asp:Label ID="lbl" runat="server" Style="color: #CC0000;"></asp:Label>
                </td>
            </tr>
        </table>



    </div>

    <table style="width: 100%;" cellspacing="10">
        <tr>
            <td>
                <asp:UpdatePanel ID="UP" runat="server">
                    <ContentTemplate>
                        <div class="clear1"></div>
                        <div class="large-12 medium-12 small-12">
                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                                OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowDeleting="GridView1_RowDeleting"
                                OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" PageSize="8"
                                CssClass="GridViewStyle" Width="100%">
                                <Columns>
                                        <asp:TemplateField Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_userid" runat="server" Text='<%# Eval("Created_By")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                   
                                    <asp:TemplateField HeaderText="CancellationChrg">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CancellationChrg" runat="server" Text='<%# Eval("Cancellation_Chrg")%>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txt_CancellationChrg" runat="server" Text='<%# Eval("Cancellation_Chrg")%>'></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Type">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Type" runat="server" Text='<%# Eval("Type")%>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="DropDownList1" runat="server">
                                                <asp:ListItem Value="F">Fixed (F)</asp:ListItem>
                                                <asp:ListItem Value="P">Percentage (P)</asp:ListItem>
                                            </asp:DropDownList>                                    
                                        </EditItemTemplate>
                                    </asp:TemplateField>

                                       <asp:TemplateField HeaderText="Provider Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblProvider_Name" runat="server" Text='<%# Eval("Provider_Name")%>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtProvider_Name" runat="server" Text='<%# Eval("Provider_Name")%>'></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Created Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_CREATED_DATE" runat="server" Text='<%# Eval("CREATED_DATE")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField ShowDeleteButton="True" />
                                    <asp:CommandField ShowEditButton="True" />
                                </Columns>
                                <RowStyle CssClass="RowStyle" />
                                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                <PagerStyle CssClass="PagerStyle" />
                                <SelectedRowStyle CssClass="SelectedRowStyle" />
                                <HeaderStyle CssClass="HeaderStyle" />
                                <EditRowStyle CssClass="EditRowStyle" />
                                <AlternatingRowStyle CssClass="AltRowStyle" />
                            </asp:GridView>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UP">
                    <ProgressTemplate>
                        <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                        </div>
                        <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                            Please Wait....<br />
                            <br />
                            <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                            <br />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>
    </table>--%>


    <div class="row">
         <div class="col-md-12"  >
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Bus Cancellation Charges</h3>
                    </div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Cancellation Charge</label>
                                    <asp:TextBox runat="server" ID="mk" class="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFVMK" runat="server" ControlToValidate="mk" ErrorMessage="*"
                                        Display="dynamic"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Type</label>
                                    <asp:DropDownList ID="ddl_mktyp" runat="server" class="form-control">
                                        <asp:ListItem Value="F">Fixed (F)</asp:ListItem>
                                        <asp:ListItem Value="P">Percentage (P)</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Provider Name</label>
                                    <asp:DropDownList ID="ddl_providername" runat="server" class="form-control">
                                        <asp:ListItem Value="RB">RedBus</asp:ListItem>
                                        <asp:ListItem Value="GS">GujratService</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <br />

                                    <asp:Button ID="Button2" runat="server" OnClick="btnAdd_Click" Text="Add New" CssClass="button buttonBlue" ControlToValidate="mk"  />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <br />
                                    <asp:Label ID="lbl" runat="server" Style="color: #CC0000;"></asp:Label>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="background-color: #fff; overflow: auto;" runat="server">
                            <div align="center">
                                <table style="width: 100%;" cellspacing="10">
                                    <tr>
                                        <td>
                                            <asp:UpdatePanel ID="UP" runat="server">
                                                <ContentTemplate>
                                                    <div class="clear1"></div>
                                                    <div class="col-sm-12">
                                                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                                                            OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowDeleting="GridView1_RowDeleting"
                                                            OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" PageSize="8"
                                                           CssClass="table">
                                                            <Columns>
                                                                <asp:TemplateField Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_userid" runat="server" Text='<%# Eval("Id")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="CancellationChrg">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_CancellationChrg" runat="server" Text='<%# Eval("Cancellation_Chrg")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txt_CancellationChrg" runat="server" Text='<%# Eval("Cancellation_Chrg")%>'></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Type">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_Type" runat="server" Text='<%# Eval("Type")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate>
                                                                        <asp:DropDownList ID="DropDownList1" runat="server">
                                                                            <asp:ListItem Value="F">Fixed (F)</asp:ListItem>
                                                                            <asp:ListItem Value="P">Percentage (P)</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </EditItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Provider Name">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblProvider_Name" runat="server" Text='<%# Eval("Provider_Name")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                   
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Created Date">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_CREATED_DATE" runat="server" Text='<%# Eval("CREATED_DATE")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:CommandField ShowDeleteButton="True" />
                                                                <asp:CommandField ShowEditButton="True" />
                                                            </Columns>
                                                            <RowStyle CssClass="RowStyle" />
                                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                            <PagerStyle CssClass="PagerStyle" />
                                                            <SelectedRowStyle CssClass="SelectedRowStyle" />
                                                            <HeaderStyle CssClass="HeaderStyle" />
                                                            <EditRowStyle CssClass="EditRowStyle" />
                                                            <AlternatingRowStyle CssClass="AltRowStyle" />
                                                        </asp:GridView>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UP">
                                                <ProgressTemplate>
                                                    <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                                    </div>
                                                    <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                                        Please Wait....<br />
                                                        <br />
                                                        <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                                        <br />
                                                    </div>
                                                </ProgressTemplate>
                                            </asp:UpdateProgress>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</asp:Content>

