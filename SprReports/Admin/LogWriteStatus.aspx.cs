﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SprReports_Admin_LogWriteStatus : System.Web.UI.Page
{
    string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            BindGridView();
        }

    }
    protected void BindGridView()
    {
        try
        {
            using (SqlCommand sqlcmd = new SqlCommand())
            {
                SqlConnection con = new SqlConnection(constr);
                sqlcmd.Connection = con;
                con.Open();
                sqlcmd.CommandTimeout = 900;
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandText = "SP_Tbl_LogAirlineStatus";
                SqlDataAdapter da = new SqlDataAdapter(sqlcmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                con.Close();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GridView1.DataSource = ds;
                    GridView1.DataBind();
                }
                else
                {
                    ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                    GridView1.DataSource = ds;
                    GridView1.DataBind();
                    int columncount = GridView1.Rows[0].Cells.Count;
                    GridView1.Rows[0].Cells.Clear();
                    GridView1.Rows[0].Cells.Add(new TableCell());
                    GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                    GridView1.Rows[0].Cells[0].Text = "No Records Found";
                }
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }


    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGridView();
    }
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        BindGridView();
    }
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        BindGridView();
    }
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            SqlConnection con = new SqlConnection(constr);
            string aa = GridView1.DataKeys[e.RowIndex].Value.ToString();
            GridViewRow row = (GridViewRow)GridView1.Rows[e.RowIndex];
            bool BolRTF = (row.Cells[0].Controls[0] as CheckBox).Checked;
            bool BOlStatus = (row.Cells[2].Controls[0] as CheckBox).Checked;
            GridView1.EditIndex = -1;
            BindGridView();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
          
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }
    protected void chk_Status_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            int a = 0;
            string UserID = Session["UID"].ToString();
            string IPAddress = Request.ServerVariables["REMOTE_ADDR"];
            GridViewRow CHK_Sts = (GridViewRow)(((CheckBox)sender).NamingContainer);
            CheckBox chk_Status = (CheckBox)CHK_Sts.FindControl("chk_Status");
            Label lbl_Counter = (Label)CHK_Sts.FindControl("lbl_counter");
            using (SqlCommand sqlcmd = new SqlCommand())
            {
                SqlConnection con = new SqlConnection(constr);
                sqlcmd.Connection = con;
                con.Open();
                sqlcmd.CommandTimeout = 900;
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandText = "SP_Tbl_LogAirlineStatusUpdated";
                sqlcmd.Parameters.Add("@ID", SqlDbType.Int).Value = Convert.ToInt32(lbl_Counter.Text.ToString());
                sqlcmd.Parameters.Add("@STATUS", SqlDbType.VarChar).Value = chk_Status.Checked.ToString();
                sqlcmd.Parameters.Add("@UPDATEBY", SqlDbType.VarChar).Value = UserID;
             
              a=  sqlcmd.ExecuteNonQuery();
              if (a > 0)
              {
                  string message = "Update Successfully";
                  ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Update Successfully');", true);
              }
              con.Close();
               
            }
            System.Threading.Thread.Sleep(1000);
            BindGridView();

        }
        catch (Exception ex)
        {
            string message = "Something Went Wrong!! Try again after sometimes";      
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("<script type = 'text/javascript'>");
            sb.Append("window.onload=function(){");
            sb.Append("alert('");
            sb.Append(message);
            sb.Append("')};");
            sb.Append("</script>");
            ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb.ToString());      
            clsErrorLog.LogInfo(ex);
        }
    }
}