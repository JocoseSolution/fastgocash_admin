﻿Imports System.Data
Partial Class HtlRefundInProcces
    Inherits System.Web.UI.Page
    Private ST As New HotelDAL.HotelDA()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            If Session("UID") Is Nothing Then
                Response.Redirect("~/Login.aspx", True)
            End If
            If Not IsPostBack Then
                BindGrid()
            End If
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try
    End Sub

    Private Sub BindGrid()
        Try
            InproccesGrd.DataSource = ST.HtlRefundDetail(StatusClass.InProcess.ToString(), "", Session("UID").ToString, "InproceGET", "")
            InproccesGrd.DataBind()
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try


    End Sub
    Protected Sub btnRemark_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemark.Click
        Try
            'Update Status and Remark in CancellationIntl Table after Reject
            Dim i As Integer = ST.HtlRefundUpdates(StatusClass.Rejected.ToString(), Request("OrderIDS"), Session("UID").ToString, "RejInproce", Request("txtRemark").Trim())
            If i > 0 Then
                Page.ClientScript.RegisterStartupScript(GetType(Page), "MessagePopUp", "alert('Reject successfully.'); ", True)
                BindGrid()
                Try
                    ''''Send Mail and SMS Start
                    Dim HtlDetailsDs As New DataSet()
                    HtlDetailsDs = ST.htlintsummary(Request("OrderIDS"), "Ticket")

                    Dim objmail As New HotelBAL.HotelSendMail_Log()
                    objmail.SendEmailForCancelAndReject(Request("OrderIDS"), HtlDetailsDs.Tables(0).Rows(0)("BookingID").ToString(), HtlDetailsDs.Tables(0).Rows(0)("HotelName").ToString(), HtlDetailsDs.Tables(0).Rows(0)("LoginID").ToString(), HotelShared.HotelStatus.HOTEL_REJECT.ToSting(), "Cancellation Reject")

                    Dim objSMSAPI As New SMSAPI.SMS
                    Dim objSqlNew As New SqlTransactionNew
                    Dim smstext As String = ""
                    Dim SmsCrd As DataTable
                    Dim objDA As New SqlTransaction
                    SmsCrd = objDA.SmsCredential(SMS.HOTELBOOKING.ToString()).Tables(0)
                    Dim smsStatus As String = ""
                    Dim smsMsg As String = ""
                    If SmsCrd.Rows.Count > 0 AndAlso SmsCrd.Rows(0)("Status") = True Then
                        smsStatus = objSMSAPI.SendHotelSms(Request("OrderIDS"), "", HtlDetailsDs.Tables(1).Rows(0)("GPhoneNo").ToString().Trim(), "", "", "", "CancelReject", smstext, SmsCrd)
                        objSqlNew.SmsLogDetails(Request("OrderIDS"), HtlDetailsDs.Tables(1).Rows(0)("GPhoneNo").ToString().Trim(), smstext, smsStatus)
                    End If
                    ''''Send Mail and SMS End
                Catch ex As Exception
                    HotelDAL.HotelDA.InsertHotelErrorLog(ex, " Send SMS Support Refund InProcess")
                End Try
            End If
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try
    End Sub

    Protected Sub lnkupdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lnkupdate As String = CType(sender, LinkButton).CommandArgument.ToString()
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "Window", "window.open('HtlRefundUpdate.aspx?OrderID=" & lnkupdate & "','Print','scrollbars=yes,width=1100px,height=490,top=20,left=150');", True)
        BindGrid()
    End Sub
End Class
