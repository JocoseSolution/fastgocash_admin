﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;

public partial class SprReports_Accounts_IRCTCCharges : System.Web.UI.Page
{
    DataTable dt = new DataTable();
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    #region New Concept Code
    private SqlTransactionDom STDom = new SqlTransactionDom();
    private SqlDataAdapter adap;
    string msgout = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
        DivMsg.InnerHtml = "";
        DivMsgExclued.InnerHtml = "";
        if (!string.IsNullOrEmpty(Convert.ToString(Session["UID"])))
        {
            try
            {
                if (!IsPostBack)
                {
                    ddl_ptype.AppendDataBoundItems = true;
                    ddl_ptype.Items.Clear();
                    //Dim item As New ListItem("All Type", "0")
                    //ddl_ptype.Items.Insert(0, item)
                    ddl_ptype.DataSource = STDom.GetAllGroupType().Tables[0];
                    ddl_ptype.DataTextField = "GroupType";
                    ddl_ptype.DataValueField = "GroupType";
                    ddl_ptype.DataBind();
                    ddl_ptype.Items.Insert(0, new ListItem("-- Select Type --", "ALL"));
                    BindGrid();
                }


            }
            catch (Exception ex)
            {
                clsErrorLog.LogInfo(ex);
            }
        }
        else
        {
            Response.Redirect("~/Login.aspx");
        }
    }


    protected void BtnSubmit_Click(object sender, EventArgs e)
    {
        #region Insert
        try
        {           
            int Id = 0;
            string ServiceCode = ddlCardType.SelectedValue;
            string PaymentMode = ddlCardType.SelectedItem.Text;
            string Charges = txtPgCharges.Text.Trim();
            string ChargesType = ddlChargeType.SelectedValue;
            string GroupType= ddl_ptype.SelectedValue;
            string AgentId = "";// Request["hidtxtAgencyName"] == "Agency Name or ID" ? "" : Request["hidtxtAgencyName"].Trim();
            string SelectedUserId = hfCustomerId.Value;
            string ActionType = "insert";
            string AddMsg = "";
            msgout = "";
            DivMsg.InnerHtml = "";
            if (!string.IsNullOrEmpty(SelectedUserId))
                {
                 #region Insert PG Charges Agent UserId Wise
                int flag = 0;
                    int len = SelectedUserId.Split(',').Length;
                    for (int i = 0; i < len - 1; i++)
                    {
                        msgout = "";
                        GroupType = "";
                        AgentId = SelectedUserId.Split(',')[i];
                        flag = InsertAndUpdate(Id, ServiceCode, PaymentMode, Charges, ChargesType, GroupType, AgentId, ActionType);                       
                        AddMsg = AddMsg + AgentId + "-" + msgout + ".<br />";
                    }
                    hfCustomerId.Value = "";
                    if (!string.IsNullOrEmpty(AddMsg))
                    {                      
                        DivMsg.InnerHtml = AddMsg;                      
                        hidActionType.Value = "select";
                        BindGrid();
                    }
                    else
                    {                       
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('try again');", true);
                        hidActionType.Value = "select";
                        BindGrid();
                    }
                #endregion Insert PG Charges AgentWise
                }
            else
                {
                 #region Insert PG Charges Agent Type Wise
                    DivMsg.InnerHtml = AddMsg;
                    AgentId = "";
                    msgout = "";                    
                    int flag = InsertAndUpdate(Id, ServiceCode, PaymentMode, Charges, ChargesType, GroupType, AgentId, ActionType);
                    AddMsg = msgout;                    
                    hfCustomerId.Value = "";                   
                    DivMsg.InnerHtml = AddMsg;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert(" + AddMsg + ");", true);                   
                    hidActionType.Value = "select";
                    BindGrid();
                #endregion Insert PG Charges Agent Type Wise
            }
            #endregion            
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + ex.Message + "');window.location='PGCharges.aspx'; ", true);
            return;
        }
        #endregion
    }

    private int InsertAndUpdate(int Id, string ServiceCode, string PaymentMode, string Charges, string ChargesType, string GroupType, string AgentId, string ActionType)
    {       
        int flag = 0;
        string ActionBy = Convert.ToString(Session["UID"]);
       // ServiceCode = "IRCTC";
        Charges = Regex.Replace(Charges, @"\s+", "");
        if (string.IsNullOrEmpty(Regex.Replace(Charges, @"\s+", "")))
        {
            Charges = "0";
        }
            try
        {
            SqlCommand cmd = new SqlCommand("SP_IRCTC_TransChargeMaster", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", Id);
            cmd.Parameters.AddWithValue("@ServiceCode", ServiceCode);
            //cmd.Parameters.AddWithValue("@PaymentMode", PaymentMode);            
            cmd.Parameters.AddWithValue("@Charges", Charges);
            cmd.Parameters.AddWithValue("@ChargesType", ChargesType);
            cmd.Parameters.AddWithValue("@GroupType", GroupType);
            cmd.Parameters.AddWithValue("@AgentId", AgentId);
            cmd.Parameters.AddWithValue("@ActionBy", ActionBy);
            cmd.Parameters.AddWithValue("@Action", ActionType);
            cmd.Parameters.Add("@Msg", SqlDbType.VarChar, 100);
            cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
            if (con.State == ConnectionState.Closed)
                con.Open();
            flag = cmd.ExecuteNonQuery();
            con.Close();
            msgout = cmd.Parameters["@Msg"].Value.ToString();
        }
        catch (Exception ex)
        {
            con.Close();
            clsErrorLog.LogInfo(ex);
        }
        return flag;

    }
    public void BindGrid()
    {
        try
        {
            string ServiceCode = "", ChargesType = "", GroupType = "", AgentId = "";            
            #region

            if (hidActionType.Value == "search")
            {
                #region search value set 
                ServiceCode= ddlCardType.SelectedValue;
                ChargesType= ddlChargeType.SelectedValue;
                GroupType = ddl_ptype.SelectedValue;
                string SelectedUserId = hfCustomerId.Value;
               
                DivMsg.InnerHtml = "";

                if (!string.IsNullOrEmpty(SelectedUserId))
                {
                    string JoinAgentId = "";
                    int len = SelectedUserId.Split(',').Length;//> 1
                    for (int i = 0; i < len - 1; i++)
                    {
                        msgout = "";
                        GroupType = "";
                        //AgentId = SelectedUserId.Split(',')[i];
                        JoinAgentId = JoinAgentId + "'" + SelectedUserId.Split(',')[i] + "',";

                    }
                    JoinAgentId = JoinAgentId.TrimEnd(',');
                    AgentId = JoinAgentId; //"(" + JoinAgentId + ")";
                                           // hfCustomerId.Value = "";                   
                }
                else
                {
                    hfCustomerId.Value = "";
                }

                #endregion
            }
            #endregion
            
            grd_P_IntlDiscount.DataSource = GetRecord(ServiceCode, ChargesType, GroupType, AgentId);
            grd_P_IntlDiscount.DataBind();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }
    public DataTable GetRecord(string ServiceCode, string ChargesType, string GroupType, string AgentId)
    {
        //Id, ServiceCode, PaymentMode, Charges, ChargesType, ValidTo, ValidFrom, Status, CreatedBy, CreatedDate, UpdatedDate, UpdatedBy, GroupType, 
        //                 UserId, AgencyId, AgencyName

        DataTable dt = new DataTable();
        try
        {
            string ActionType = hidActionType.Value;
            if (con.State == ConnectionState.Closed)
                con.Open();                   
            adap = new SqlDataAdapter("SP_IRCTC_TransChargeMaster", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.SelectCommand.Parameters.AddWithValue("@ServiceCode", ServiceCode);
            adap.SelectCommand.Parameters.AddWithValue("@ChargesType", ChargesType);
            adap.SelectCommand.Parameters.AddWithValue("@GroupType", GroupType);
            adap.SelectCommand.Parameters.AddWithValue("@AgentId", AgentId);
            adap.SelectCommand.Parameters.AddWithValue("@action", ActionType);
            adap.SelectCommand.Parameters.Add("@Msg", SqlDbType.VarChar, 100);
            adap.SelectCommand.Parameters["@Msg"].Direction = ParameterDirection.Output;
            adap.Fill(dt);
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        finally
        {
            con.Close();
            adap.Dispose();
        }
        return dt;
    }


    protected void grd_P_IntlDiscount_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            DivMsg.InnerHtml = "";
            grd_P_IntlDiscount.EditIndex = e.NewEditIndex;
            //ActionTypeGrid = "select";
            BindGrid();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }

    }

    protected void grd_P_IntlDiscount_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {
            DivMsg.InnerHtml = "";
            grd_P_IntlDiscount.EditIndex = -1;
            //ActionTypeGrid = "select";
            BindGrid();

        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }

    protected void grd_P_IntlDiscount_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            DivMsg.InnerHtml = "";
            int result = 0;
            Label lblSNo = (Label)(grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("lblId"));
            int Id = Convert.ToInt32(lblSNo.Text.Trim().ToString());  
            TextBox txtChargesGrd = (TextBox)(grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("txtChargesGrd"));
            string ChargesGrd = Convert.ToString(txtChargesGrd.Text);
            DropDownList ddlChargesTypeGrd = (DropDownList)grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("ddlChargesTypeGrd");
            string ChargesTypeGrd = ddlChargesTypeGrd.SelectedValue;
            string ActionType = "update";
            if (!string.IsNullOrEmpty(Convert.ToString(Id))  && !string.IsNullOrEmpty(ChargesGrd) && !string.IsNullOrEmpty(ChargesTypeGrd))
            {
               string ServiceCode = "IRCTC";
                result = InsertAndUpdate(Id, ServiceCode, ServiceCode, ChargesGrd, ChargesTypeGrd, "", "", ActionType);
                grd_P_IntlDiscount.EditIndex = -1;
                // ActionTypeGrid = "select";
                BindGrid();

                if (result > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('Successfully updated.');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('try again.');", true);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(ChargesGrd))
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('Enter PG Charges.');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('Try again.');", true);
                }
            }
            
           
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('" + ex.Message + "');", true);
        }
    }

    protected void OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            SqlCommand cmd=null;
            int flag = 0;
            string ActionBy = Convert.ToString(Session["UID"]);
            try
            {
                Label lblSNo = (Label)(grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("lblId"));
                int Id = Convert.ToInt16(lblSNo.Text.Trim().ToString());
                cmd = new SqlCommand("SP_IRCTC_TransChargeMaster", con);////SpSearchResultBlock
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", Id);
                cmd.Parameters.AddWithValue("@ActionBy", ActionBy);
                cmd.Parameters.AddWithValue("@Action", "delete");
                cmd.Parameters.Add("@Msg", SqlDbType.VarChar, 100);
                cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
                if (con.State == ConnectionState.Closed)
                    con.Open();
                flag = cmd.ExecuteNonQuery();
                cmd.Dispose();
                con.Close();
            }
            catch (SqlException ex)
            {
                con.Close();
                cmd.Dispose();               
                clsErrorLog.LogInfo(ex);
            }
            //ActionTypeGrid = "select";
            BindGrid();
            if (flag > 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Record successfully deleted.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Problen in deleting record.');", true);
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }


    protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grd_P_IntlDiscount.PageIndex = e.NewPageIndex;
        //ActionTypeGrid = "select";        
        this.BindGrid();
    }   
    protected void BtnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            DivMsg.InnerHtml = "";
            hidActionType.Value = "search";
            BindGrid();


        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + ex.Message + "');window.location='PGCharges.aspx'; ", true);
            return;
        }

    }

   
    

    
}