﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="PGCharges.aspx.cs" Inherits="SprReports_Accounts_PGCharges" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="../../chosen/jquery-1.6.1.min.js" type="text/javascript"></script>
    <script src="../../chosen/chosen.jquery.js" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
    <div class="row">
        <div class="col-md-12 " >
            <div class="page-wrapperss">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Account > Payment Gateway Transaction Charges</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Payment Mode :</label>
                                    <%--<asp:DropDownList ID="ddlCardType" runat="server" AutoPostBack="true"
                                        CssClass="form-control" OnSelectedIndexChanged="ddlCardType_SelectedIndexChanged">
                                    </asp:DropDownList>--%>
                                    <asp:DropDownList ID="ddlCardType" runat="server" CssClass="form-control"  TabIndex="1">
                                        <asp:ListItem Value="0">--Select--</asp:ListItem>
                                         <asp:ListItem Value="netbanking">Net Banking</asp:ListItem>
                                        <asp:ListItem Value="debitcard">Debit Card</asp:ListItem>
                                        <asp:ListItem Value="creditcard">Credit Card</asp:ListItem>
                                        <asp:ListItem Value="cashcard">Cash Card</asp:ListItem>  
					<asp:ListItem Value="Paytm">Paytm</asp:ListItem>
					<asp:ListItem Value="Freecharge">Freecharge</asp:ListItem>                                             
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Charge Type :</label>
                                    <asp:DropDownList ID="ddlChargeType" runat="server" CssClass="form-control"  TabIndex="2">
                                         <asp:ListItem Value="0" Selected="true">--Select--</asp:ListItem>
                                        <asp:ListItem Value="P">Percentage</asp:ListItem>
                                        <asp:ListItem Value="F">Fixed</asp:ListItem>                                        
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                     <label for="exampleInputPassword1">Transaction Charges</label>
                                    <asp:TextBox ID="txtPgCharges" runat="server" CssClass="form-control" onkeypress="return keyRestrict(event,'.0123456789');" oncopy="return false" onpaste="return false" MaxLength="7"  TabIndex="3"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">GroupType :</label>
                                    <asp:DropDownList ID="ddl_ptype" CssClass="form-control" runat="server" AppendDataBoundItems="true" TabIndex="4">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="row">

                           
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Agent Id  </label>
                                    <asp:TextBox ID="txtSearch" runat="server" oncopy="return false" onpaste="return false" oncut="return false" CssClass="form-control" onkeydown="return checkShortcut();" TabIndex="5"></asp:TextBox>
                                    <asp:HiddenField ID="hfCustomerId" runat="server" />
                                    <%--<span onclick="ClearRec();" >Clear Value</span>--%>
                                </div>
                                <div id="content"></div>
                            </div>

                             <div class="col-md-3" style="display: none;">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Exclude Agent Id  </label>
                                    <asp:TextBox ID="txtSearchExclude" runat="server" oncopy="return false" onpaste="return false" oncut="return false" CssClass="form-control" onkeydown="return checkShortcut();" TabIndex="6"></asp:TextBox>
                                    <asp:HiddenField ID="hfCustomerIdExclude" runat="server" />
                                    <%--<span onclick="ClearRec();" >Clear Value</span>--%>
                                </div>
                                <div id="contentExclude"></div>
                            </div>

                            <div class="col-md-3" >
                                <div class="form-group">
                                   <asp:Button ID="BtnSubmit" runat="server" Text="Submit" OnClick="BtnSubmit_Click" CssClass="button buttonBlue" TabIndex="7" OnClientClick="return Check();" /> 
                                    
                                </div>
                            </div>
                           
                                      <div class="col-md-3" >
                                <div class="form-group">
                                     <asp:Button ID="BtnSearch" runat="server" Text="Search" CssClass="button buttonBlue" OnClick="BtnSearch_Click"  TabIndex="8" />
                                    
                                </div>
                            </div>
                           

                        </div>
                        <div class="clear"></div>                        
                        <div class="row">

                            <div class="col-md-12">
                                <div id="DivMsg" runat="server" style="color: red;"></div>      
                                <br />
                                <div id="DivMsgExclued" runat="server" style="color: red;"></div>                            
                            </div>
                        </div>


                        <div class="row">
                             <div class="col-md-12">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="background-color: #fff; overflow: auto; max-height: 500px;">
                                    <ContentTemplate>
                                        <asp:GridView ID="grd_P_IntlDiscount" runat="server" AutoGenerateColumns="false"
                                            CssClass="table" GridLines="None" Width="100%" PageSize="100" OnRowCancelingEdit="grd_P_IntlDiscount_RowCancelingEdit"
                                            OnRowEditing="grd_P_IntlDiscount_RowEditing" OnRowUpdating="grd_P_IntlDiscount_RowUpdating" OnRowDeleting="OnRowDeleting" AllowPaging="true"
                                            OnPageIndexChanging="OnPageIndexChanging">
                                            <Columns>

                                                  <%--Id	PaymentCode	PaymentMode	Charges	ChargesType	ValidTo	ValidFrom	Status	CreatedBy	CreatedDate	UpdatedDate	UpdatedBy--%>
                                                <asp:TemplateField HeaderText="PaymentMode">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblId" runat="server" Visible="false" Text='<%#Eval("Id") %>'></asp:Label>
                                                        <asp:Label ID="lbl_PaymentMode" runat="server" Text='<%#Eval("PaymentMode") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Group_Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGroupType" runat="server" Text='<%#Eval("GroupType") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="AgencyName">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAgentId" runat="server" Text='<%#Eval("AgencyName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField> 
                                                 <asp:TemplateField HeaderText="Charge_Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblChargesType" runat="server" Text='<%#Eval("CType") %>'></asp:Label>
                                                    </ItemTemplate>
                                                      <EditItemTemplate>                                                                                                                
                                                                <asp:DropDownList ID="ddlChargesTypeGrd" runat="server" Width="150px" DataValueField='<%#Eval("ChargesType")%>' SelectedValue='<%#Eval("ChargesType")%>'>                                       
                                                                 <asp:ListItem Value="P">Percentage</asp:ListItem>
                                                                  <asp:ListItem Value="F">Fixed</asp:ListItem>   
                                                                </asp:DropDownList>
                                                    </EditItemTemplate>  
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Charges">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCharges" runat="server" Text='<%#Eval("Charges") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtChargesGrd" runat="server" Text='<%#Eval("Charges") %>'
                                                            onKeyPress="return keyRestrict(event,'.0123456789');" Width="60px" MaxLength="7" BackColor="#ffff66"></asp:TextBox>                                                        
                                                    </EditItemTemplate>
                                                </asp:TemplateField>                                                
                                                <asp:TemplateField HeaderText="CreatedBy">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCreatedBy" runat="server" Text='<%#Eval("CreatedBy") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="CreatedDate">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCreatedDate" runat="server" Text='<%#Eval("CreatedDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField> 
                                                <asp:TemplateField HeaderText="UpdatedDate">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUpdatedDate" runat="server" Text='<%#Eval("UpdatedDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField> 
                                                <asp:TemplateField HeaderText="UpdatedBy">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUpdatedBy" runat="server" Text='<%#Eval("UpdatedBy") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                
                                                <asp:TemplateField HeaderText="EDIT">
                                                    <ItemTemplate>
                                                        <asp:Button ID="lnledit" runat="server" Text="Edit" CommandName="Edit" Font-Bold="true"
                                                            CssClass="newbutton_2" />
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:Button ID="lnlupdate" runat="server" Text="Update" CommandName="Update" Font-Bold="true" CssClass="newbutton_2" />
                                                        <asp:Button ID="lnlcancel" runat="server" Text="Cancel" CommandName="Cancel" Font-Bold="true"
                                                            CssClass="newbutton_2" />
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <ItemTemplate>
                                                        <%--<asp:Button ID="btn_delete" CssClass="newbutton_2" runat="server" Text="Delete" CommandName="Delete" Font-Bold="true" />--%>
                                                        <asp:Button ID="btn_delete" CssClass="newbutton_2" runat="server" Text="Delete" CommandName="Delete" OnClientClick="if(!confirm('Do you want to delete?')){ return false; };" Font-Bold="true" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="RowStyle" />
                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                            <PagerStyle CssClass="PagerStyle" />
                                            <SelectedRowStyle CssClass="SelectedRowStyle" />
                                            <HeaderStyle CssClass="HeaderStyle" />
                                            <EditRowStyle CssClass="EditRowStyle" />
                                            <AlternatingRowStyle CssClass="AltRowStyle" />
                                            <EmptyDataTemplate>No records Found</EmptyDataTemplate>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>


                                <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                    <ProgressTemplate>
                                        <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                        </div>
                                        <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                            Please Wait....<br />
                                            <br />
                                            <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                            <br />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hidActionType" runat="server" Value="select" />
    </div>

    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/change.min.js") %>"></script>
    <%--<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/search4.js") %>"></script>     --%>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            //  Autocomplete  Nationality
            var countrycode = $('.Nationality').each(function () {
                $(this).autocomplete({
                    source: function (e, t) {
                        $.ajax({
                            url: UrlBase + "CitySearch.asmx/GetCountryCd",
                            data: "{ 'country': '" + e.term + "', maxResults: 10 }",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (e) {
                                t($.map(e.d, function (e) {
                                    var t = e.CountryName + "(" + e.CountryCode + ")";
                                    var n = e.CountryCode;
                                    return {
                                        label: t,
                                        value: t,
                                        id: n
                                    }
                                }))
                            },
                            error: function (e, t, n) {
                                alert(t)
                            }
                        })
                    },
                    autoFocus: true,
                    minLength: 3,
                    select: function (t, n) {
                        $(this).next().val(n.item.id)
                    }
                });
            });

        });

        function Check() {
            if ($("#ctl00_ContentPlaceHolder1_ddlCardType").val() == "0") {
                alert("Select Payment Mode.");
                $("#ctl00_ContentPlaceHolder1_ddlCardType").focus();
                return false;
            }            
            if ($("#ctl00_ContentPlaceHolder1_ddlChargeType").val() == "0") {
                alert("Select Charge Type.");
                $("#ctl00_ContentPlaceHolder1_ddlChargeType").focus();
                return false;
            }
            if ($("#ctl00_ContentPlaceHolder1_txtPgCharges").val() == "") {
                alert("Select PG Transaction Charges.");
                $("#ctl00_ContentPlaceHolder1_txtPgCharges").focus();
                return false;
            }
        }  

function getKeyCode(e) {
             if (window.event)
                 return window.event.keyCode;
             else if (e)
                 return e.which;
             else
                 return null;
         }
         function keyRestrict(e, validchars) {
             var key = '', keychar = '';
             key = getKeyCode(e);
             if (key == null) return true;
             keychar = String.fromCharCode(key);
             keychar = keychar.toLowerCase();
             validchars = validchars.toLowerCase();
             if (validchars.indexOf(keychar) != -1)
                 return true;
             if (key == null || key == 0 || key == 8 || key == 9 || key == 13 || key == 27)
                 return true;
             return false;
         }   

    </script>


    <script type="text/javascript">
        function checkShortcut() {
            //if (event.keyCode == 8 || event.keyCode == 13 || event.keyCode == 46) {
            //    return false;
            //}
        }
        function ClearRec() {
            $("#ctl00_ContentPlaceHolder1_txtSearch").val("");
            //$("#ctl00_ContentPlaceHolder1_hfCustomerId").val("");
        }

        function SetAndRemove(UserId) {
            //$("#ctl00_ContentPlaceHolder1_txtSearch").val("");
            //$("#ctl00_ContentPlaceHolder1_hfCustomerId").val("");

            var maincontent = "";
            var AgentId = "";
            var UserCount = $("#[id*=hfCustomerId]")[0].value.split(",").length;
            if (UserCount > 1) {
                var max = UserCount - 2
                for (var n = 0; n <= UserCount - 2; n++) {
                    if ($("#[id*=hfCustomerId]")[0].value.split(",")[n] != UserId) {
                        if (max == 0 || n == max) {
                            maincontent = maincontent + "<span onclick=\"SetAndRemove('" + $("#[id*=hfCustomerId]")[0].value.split(",")[n] + "')\">" + $("#[id*=hfCustomerId]")[0].value.split(",")[n] + "&nbsp<img src='../../Images/close.gif' />&nbsp</span>";
                            AgentId = AgentId + $("#[id*=hfCustomerId]")[0].value.split(",")[n] + ',';
                        }
                        else {
                            maincontent = maincontent + "<span onclick=\"SetAndRemove('" + $("#[id*=hfCustomerId]")[0].value.split(",")[n] + "')\">" + $("#[id*=hfCustomerId]")[0].value.split(",")[n] + "&nbsp<img src='../../Images/close.gif' />&nbsp,</span>";
                            AgentId = AgentId + $("#[id*=hfCustomerId]")[0].value.split(",")[n] + ',';
                        }
                    }
                }
                document.getElementById("content").innerHTML = maincontent;
                $("#ctl00_ContentPlaceHolder1_hfCustomerId").val(AgentId);
            }
        }

        $(document).ready(function () {
            $("#<%=txtSearch.ClientID %>").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        // url: ~/AgencySearch.asmx/GetCustomers
                        //data: "{ 'prefix': '" + request.term + "'}",
                        url: '<%=ResolveUrl("~/AgencySearch.asmx/FetchAgencyList") %>',
                        data: "{ 'city': '" + request.term + "', maxResults: 10 }",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {

                                    label: item.Agency_Name + "(" + item.User_Id + ")",
                                    val: item.User_Id
                                    //label: item.split('-')[0],
                                    //val: item.split('-')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            //alert(response.responseText);
                        },
                        failure: function (response) {
                            // alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    var text = this.value.split(/,\s*/);
                    //debugger
                    //text.pop();
                    //text.push(i.item.value);
                    //text.push("");
                    //this.value = text.join(", ");
                    this.value = "";
                    var value = $("[id*=hfCustomerId]").val().split(/,\s*/);

                    var maincontent = "";
                    value.pop();
                    value.push(i.item.val);
                    value.push("");
                    $("#[id*=hfCustomerId]")[0].value = value.join(",");
                    var UserCount = $("#[id*=hfCustomerId]")[0].value.split(",").length;
                    if (UserCount > 1) {
                        var max = UserCount - 2;
                        for (var n = 0; n <= UserCount - 2; n++) {
                            if (max == 0 || n == max) {
                                maincontent = maincontent + "<span onclick=\"SetAndRemove('" + $("#[id*=hfCustomerId]")[0].value.split(",")[n] + "')\">" + $("#[id*=hfCustomerId]")[0].value.split(",")[n] + "&nbsp<img src='../../Images/close.gif' />&nbsp</span>";
                            }
                            else {
                                maincontent = maincontent + "<span onclick=\"SetAndRemove('" + $("#[id*=hfCustomerId]")[0].value.split(",")[n] + "')\">" + $("#[id*=hfCustomerId]")[0].value.split(",")[n] + "&nbsp<img src='../../Images/close.gif' />&nbsp,</span>";
                            }


                        }
                        document.getElementById("content").innerHTML = maincontent;
                    }
                    return false;
                },
                minLength: 1
            });

        });
    </script>


    <script type="text/javascript">
        function checkShortcutExclude() {
            //if (event.keyCode == 8 || event.keyCode == 13 || event.keyCode == 46) {
            //    return false;
            //}
        }
        function ClearRecExclude() {
            $("#ctl00_ContentPlaceHolder1_txtSearchExclude").val("");
            //$("#ctl00_ContentPlaceHolder1_hfCustomerId").val("");
        }

        function SetAndRemoveExclude(UserId) {
            //$("#ctl00_ContentPlaceHolder1_txtSearch").val("");
            //$("#ctl00_ContentPlaceHolder1_hfCustomerId").val("");

            var maincontentExclude = "";
            var AgentIdExclude = "";
            var UserCount = $("#[id*=hfCustomerIdExclude]")[0].value.split(",").length;
            if (UserCount > 1) {
                var max = UserCount - 2
                for (var n = 0; n <= UserCount - 2; n++) {
                    if ($("#[id*=hfCustomerIdExclude]")[0].value.split(",")[n] != UserId) {
                        if (max == 0 || n == max) {
                            maincontentExclude = maincontentExclude + "<span onclick=\"SetAndRemoveExclude('" + $("#[id*=hfCustomerIdExclude]")[0].value.split(",")[n] + "')\">" + $("#[id*=hfCustomerIdExclude]")[0].value.split(",")[n] + "&nbsp<img src='../../Images/close.gif' />&nbsp</span>";
                            AgentIdExclude = AgentIdExclude + $("#[id*=hfCustomerIdExclude]")[0].value.split(",")[n] + ',';
                        }
                        else {
                            maincontentExclude = maincontentExclude + "<span onclick=\"SetAndRemoveExclude('" + $("#[id*=hfCustomerIdExclude]")[0].value.split(",")[n] + "')\">" + $("#[id*=hfCustomerIdExclude]")[0].value.split(",")[n] + "&nbsp<img src='../../Images/close.gif' />&nbsp,</span>";
                            AgentIdExclude = AgentIdExclude + $("#[id*=hfCustomerIdExclude]")[0].value.split(",")[n] + ',';
                        }
                    }
                }
                document.getElementById("contentExclude").innerHTML = maincontentExclude;
                $("#ctl00_ContentPlaceHolder1_hfCustomerIdExclude").val(AgentIdExclude);
            }
        }

        $(document).ready(function () {
            $("#<%=txtSearchExclude.ClientID %>").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        // url: ~/AgencySearch.asmx/GetCustomers
                        //data: "{ 'prefix': '" + request.term + "'}",
                        url: '<%=ResolveUrl("~/AgencySearch.asmx/FetchAgencyList") %>',
                        data: "{ 'city': '" + request.term + "', maxResults: 10 }",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {

                                    label: item.Agency_Name + "(" + item.User_Id + ")",
                                    val: item.User_Id
                                    //label: item.split('-')[0],
                                    //val: item.split('-')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            //alert(response.responseText);
                        },
                        failure: function (response) {
                            // alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    var text = this.value.split(/,\s*/);
                    //debugger
                    //text.pop();
                    //text.push(i.item.value);
                    //text.push("");
                    //this.value = text.join(", ");
                    this.value = "";
                    var value = $("[id*=hfCustomerIdExclude]").val().split(/,\s*/);

                    var maincontentExclude = "";
                    value.pop();
                    value.push(i.item.val);
                    value.push("");
                    $("#[id*=hfCustomerIdExclude]")[0].value = value.join(",");
                    var UserCount = $("#[id*=hfCustomerIdExclude]")[0].value.split(",").length;
                    if (UserCount > 1) {
                        var max = UserCount - 2;
                        for (var n = 0; n <= UserCount - 2; n++) {
                            if (max == 0 || n == max) {
                                maincontentExclude = maincontentExclude + "<span onclick=\"SetAndRemoveExclude('" + $("#[id*=hfCustomerIdExclude]")[0].value.split(",")[n] + "')\">" + $("#[id*=hfCustomerIdExclude]")[0].value.split(",")[n] + "&nbsp<img src='../../Images/close.gif' />&nbsp</span>";
                            }
                            else {
                                maincontentExclude = maincontentExclude + "<span onclick=\"SetAndRemoveExclude('" + $("#[id*=hfCustomerIdExclude]")[0].value.split(",")[n] + "')\">" + $("#[id*=hfCustomerIdExclude]")[0].value.split(",")[n] + "&nbsp<img src='../../Images/close.gif' />&nbsp,</span>";
                            }


                        }
                        document.getElementById("contentExclude").innerHTML = maincontentExclude;
                    }
                    return false;
                },
                minLength: 1
            });

        });
    </script>

    


</asp:Content>


<%--Old Page Code--%>
<%--<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-md-12">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Account > Payment Gateway Transaction Charges</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Payment Mode :</label>
                                    <asp:DropDownList ID="ddlCardType" runat="server" AutoPostBack="true"
                                        CssClass="form-control" OnSelectedIndexChanged="ddlCardType_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>                               
                            </div>
                            <div class="col-md-3">
                                 <div class="form-group">
                                    <label for="exampleInputEmail1">Charge Type :</label>
                                    <asp:DropDownList ID="ddlChargeType" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="P" Selected="true">Percentage</asp:ListItem>
                                        <asp:ListItem Value="F">Fixed</asp:ListItem>                                        
                                    </asp:DropDownList>
                                </div>
                                </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Transaction Charges</label>
                                    <asp:TextBox ID="txtPgCharges" runat="server" CssClass="form-control" onkeypress="return keyRestrict(event,'.0123456789');" MaxLength="7"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <br />                                   
                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" OnClick="btnUpdate_Click" CssClass="button buttonBlue" OnClientClick="return ValidateDetails();" />
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-12">
                                <asp:UpdatePanel ID="UP" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" CssClass="table" GridLines="None" Width="100%">
                                            <Columns>                                             
                                                <asp:BoundField DataField="PaymentMode" HeaderText="Payment Mode" ControlStyle-CssClass="textboxflight1" /> 
                                                <asp:BoundField DataField="CType" HeaderText="Charge Type" ControlStyle-CssClass="textboxflight1" />
                                                <asp:BoundField DataField="Charges" HeaderText="Charges" ControlStyle-CssClass="textboxflight1" />                                                
                                                <asp:BoundField DataField="UpdatedBy" HeaderText="Updated By" ControlStyle-CssClass="textboxflight1" /> 
                                                <asp:BoundField DataField="UpdatedDate" HeaderText="Updated Date" ControlStyle-CssClass="textboxflight1" />                                                 
                                            </Columns>                                           
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UP">
                                    <ProgressTemplate>
                                        <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                        </div>
                                        <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                            Please Wait....<br />
                                            <br />
                                            <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                            <br />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
      <script type="text/javascript">
          var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>    
     <script type="text/javascript">
         function getKeyCode(e) {
             if (window.event)
                 return window.event.keyCode;
             else if (e)
                 return e.which;
             else
                 return null;
         }
         function keyRestrict(e, validchars) {
             var key = '', keychar = '';
             key = getKeyCode(e);
             if (key == null) return true;
             keychar = String.fromCharCode(key);
             keychar = keychar.toLowerCase();
             validchars = validchars.toLowerCase();
             if (validchars.indexOf(keychar) != -1)
                 return true;
             if (key == null || key == 0 || key == 8 || key == 9 || key == 13 || key == 27)
                 return true;
             return false;
         }
         function ValidateDetails() {
             if ($("#ctl00_ContentPlaceHolder1_ddlCardType").val() == "0") {
                 alert("Plesase select payment mode");
                 $("#ctl00_ContentPlaceHolder1_ddlCardType").focus();
                 return false;
             }
           

             if ($("#ctl00_ContentPlaceHolder1_ddlChargeType").val() == "0") {
                 alert("Please select  charge type");
                 $("#ctl00_ContentPlaceHolder1_ddlChargeType").focus();
                 return false;
             }

             if ($("#ctl00_ContentPlaceHolder1_txtPgCharges").val() == "") {
                 alert("Please enter transaction charges");
                 $("#ctl00_ContentPlaceHolder1_txtPgCharges").focus();
                 return false;
             }

             var bConfirm = confirm("Are you sure want to update transaction charge ?");
             if (bConfirm) {
                 return true;
             }
             else
                 return false;
         }
    </script>
</asp:Content>--%>

 


