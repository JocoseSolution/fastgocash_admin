﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SprReports_Money_Transfer_UtilityLog : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlDataAdapter adap;
    protected SqlTransactionDom STDom = new SqlTransactionDom();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
        if (!this.IsPostBack)
        {
            BindInstantPayAPILogReport();
        }
    }

    protected void BindInstantPayAPILogReport()
    {
        DataTable dtRecord = GetInstantPayAPILog();
        BindListView(dtRecord);
    }

    private void BindListView(DataTable dtRecord)
    {
        if (dtRecord != null && dtRecord.Rows.Count > 0)
        {
            Session["TransRecord"] = dtRecord;
            PagingButtom.Visible = true;
        }
        else
        {
            Session["TransRecord"] = null;
            PagingButtom.Visible = false;
        }

        lstTransaction.DataSource = dtRecord;
        lstTransaction.DataBind();
    }

    protected void btn_result_Click(object sender, EventArgs e)
    {
        BindListView(GetInstantPayAPILog(txt_Trackid.Text, Request["From"].ToString(), Request["To"].ToString()));
    }
    protected void lstTransaction_PagePropertiesChanged(object sender, EventArgs e)
    {
        BindListView(Session["TransRecord"] as DataTable);
    }
    //protected void btn_export_Click(object sender, EventArgs e)
    //{
    //    string status = ddlstatus.SelectedValue;

    //    DataTable dtRecord = new DataTable();
    //    if (Session["TransRecord"] != null && string.IsNullOrEmpty(Request["From"].ToString()) && string.IsNullOrEmpty(Request["To"].ToString()))
    //    {
    //        dtRecord = Session["TransRecord"] as DataTable;
    //    }
    //    else
    //    {
    //        dtRecord = GetDmtTrancReportList(Request["From"].ToString(), Request["To"].ToString(), txt_Trackid.Text, txt_OrderId.Text, status);
    //    }
    //    dtRecord = GetDataorExport(dtRecord);
    //    DataSet dsExport = new DataSet();
    //    dsExport.Tables.Add(dtRecord);
    //    STDom.ExportData(dsExport, ("DMT_Trans_Report_" + DateTime.Now));
    //}

    private DataTable GetInstantPayAPILog(string trackid = null, string fromDate = null, string toDate = null)
    {
        DataTable dtDueReport = new DataTable();

        try
        {
            string newfromdate = string.Empty; string newtodate = string.Empty;

            if (!string.IsNullOrEmpty(fromDate))
            {
                newfromdate = String.Format(fromDate.Split('-')[2], 4) + "-" + String.Format(fromDate.Split('-')[1], 2) + "-" + String.Format(fromDate.Split('-')[0], 2);
            }
            if (!string.IsNullOrEmpty(toDate))
            {
                newtodate = String.Format(fromDate.Split('-')[2], 4) + "-" + String.Format(fromDate.Split('-')[1], 2) + "-" + String.Format(fromDate.Split('-')[0], 2);
            }

            adap = new SqlDataAdapter("sp_GetSMBPAPILog", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            //adap.SelectCommand.Parameters.AddWithValue("@AgentId", !string.IsNullOrEmpty(agencyid) ? agencyid.Trim() : null);
            //adap.SelectCommand.Parameters.AddWithValue("@RemtMobile", !string.IsNullOrEmpty(mobile) ? mobile.Trim() : null);
            //adap.SelectCommand.Parameters.AddWithValue("@OrderId", !string.IsNullOrEmpty(orderId) ? orderId.Trim() : null);
            adap.SelectCommand.Parameters.AddWithValue("@TrackId", !string.IsNullOrEmpty(trackid) ? trackid.Trim() : null);
            adap.SelectCommand.Parameters.AddWithValue("@FromDate", !string.IsNullOrEmpty(newfromdate) ? newfromdate.Trim() : "");
            adap.SelectCommand.Parameters.AddWithValue("@ToDate", !string.IsNullOrEmpty(newtodate) ? newtodate.Trim() + " 23:59:59" : null);
            //adap.SelectCommand.Parameters.AddWithValue("@Status", !string.IsNullOrEmpty(status) ? status.Trim() : null);
            adap.Fill(dtDueReport);

        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return dtDueReport;
    }

    //private DataTable GetDataorExport(DataTable dtTrans)
    //{
    //    DataTable mytable = new DataTable();

    //    try
    //    {
    //        mytable.Columns.Add("AgentId", typeof(string));
    //        mytable.Columns.Add("Remitter Mobile", typeof(string));
    //        mytable.Columns.Add("BenificieryId", typeof(string));
    //        mytable.Columns.Add("Benificiery Name", typeof(string));
    //        mytable.Columns.Add("Bank", typeof(string));
    //        mytable.Columns.Add("Account No", typeof(string));
    //        mytable.Columns.Add("IFSC", typeof(string));
    //        mytable.Columns.Add("Amount", typeof(string));
    //        mytable.Columns.Add("Mode", typeof(string));
    //        mytable.Columns.Add("Charged Amount", typeof(string));
    //        mytable.Columns.Add("Order Id", typeof(string));
    //        mytable.Columns.Add("Client Ref Id", typeof(string));
    //        mytable.Columns.Add("Status", typeof(string));
    //        mytable.Columns.Add("Trans Date", typeof(string));

    //        if (dtTrans != null && dtTrans.Rows.Count > 0)
    //        {
    //            for (int i = 0; i < dtTrans.Rows.Count; i++)
    //            {
    //                DataRow dr1 = mytable.NewRow();
    //                dr1 = mytable.NewRow();
    //                dr1["AgentId"] = dtTrans.Rows[i]["AgentId"].ToString();
    //                dr1["Remitter Mobile"] = dtTrans.Rows[i]["RemitterMobile"].ToString();
    //                dr1["BenificieryId"] = dtTrans.Rows[i]["BenificieryId"].ToString();
    //                dr1["Benificiery Name"] = !string.IsNullOrEmpty(dtTrans.Rows[i]["CheckedBenName"].ToString()) ? dtTrans.Rows[i]["CheckedBenName"].ToString() : dtTrans.Rows[i]["BeneficaryName"].ToString();
    //                dr1["Bank"] = !string.IsNullOrEmpty(dtTrans.Rows[i]["Bank"].ToString()) ? dtTrans.Rows[i]["Bank"].ToString() : dtTrans.Rows[i]["ReqBank"].ToString();
    //                dr1["Account No"] = dtTrans.Rows[i]["AccountNumber"].ToString();
    //                dr1["IFSC"] = dtTrans.Rows[i]["IfscCode"].ToString();
    //                dr1["Amount"] = dtTrans.Rows[i]["Amount"].ToString();
    //                dr1["Mode"] = dtTrans.Rows[i]["TxnMode"].ToString();
    //                dr1["Charged Amount"] = dtTrans.Rows[i]["charged_amt"].ToString();
    //                dr1["Order Id"] = dtTrans.Rows[i]["orderid"].ToString();
    //                dr1["Client Ref Id"] = dtTrans.Rows[i]["TrackId"].ToString();
    //                dr1["Status"] = dtTrans.Rows[i]["Status"].ToString();
    //                dr1["Trans Date"] = dtTrans.Rows[i]["UpdatedDate"].ToString();
    //                mytable.Rows.Add(dr1);
    //            }
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        ex.ToString();
    //    }

    //    return mytable;
    //}
}