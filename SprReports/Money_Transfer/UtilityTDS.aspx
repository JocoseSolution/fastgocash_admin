﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="UtilityTDS.aspx.cs" Inherits="SprReports_Money_Transfer_UtilityTDS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        th, td {
            text-align: center !important;
        }

        .deletebutton {
            display: initial !important;
            padding: 0px 10px !important;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="page-wrapperss">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Utility > TDS</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <div class="row table-responsive text-nowrap">
                                    <asp:GridView ID="utilitytds_grid" runat="server" AutoGenerateColumns="False" Width="100%"
                                        PageSize="20" OnRowCancelingEdit="utilitytds_grid_RowCancelingEdit"
                                        OnRowEditing="utilitytds_grid_RowEditing" OnRowUpdating="utilitytds_grid_RowUpdating" OnRowDataBound="utilitytds_grid_RowDataBound" AllowPaging="true"
                                        OnPageIndexChanging="utilitytds_grid_PageIndexChanging">
                                        <Columns>
                                            <asp:TemplateField HeaderText="TDS Id">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblId" runat="server" Text='<%#Eval("Id") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Type">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblType" runat="server" Text='<%#Eval("Type") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control" Style="height: 30px; width: 200px;margin-left: 10px; background: #af3e3ab8; color: #fff; border: 2px solid #000;">
                                                        <asp:ListItem Value="Percentage">Percentage</asp:ListItem>
                                                        <asp:ListItem Value="Fixed">Fixed</asp:ListItem>
                                                    </asp:DropDownList>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="TDS">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTDS" runat="server" Text='<%#Eval("TDS") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtTDS" Style="height: 30px; margin-left: 10px; width: 200px; background: #af3e3ab8; color: #fff; border: 2px solid #000;" Text='<%#Eval("TDS") %>' CssClass="form-control" onkeypress="return keyRestrict(event,'0123456789');"></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Edit">
                                                <ItemTemplate>
                                                    <asp:Button ID="lnledit" runat="server" Text="Edit" CommandName="Edit" Font-Bold="true" CssClass="newbutton_2" />
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:Button ID="lnlupdate" runat="server" Text="Update" CommandName="Update" Font-Bold="true" CssClass="newbutton_2" />
                                                    <asp:Button ID="lnlcancel" runat="server" Text="Cancel" CommandName="Cancel" Font-Bold="true" CssClass="newbutton_2" />
                                                </EditItemTemplate>
                                            </asp:TemplateField>                                            
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <p style="padding: 5px; color: red; font-weight: bold;">NO COMMISSION AVAILABLE, PLEASE ADD NEW COMMISSION !</p>
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

