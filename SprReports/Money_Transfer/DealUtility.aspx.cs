﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SprReports_Money_Transfer_DealUtility : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlTransactionDom STDom = new SqlTransactionDom();
    private SqlDataAdapter adap;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
        if (Session["User_Type"].ToString().ToUpper() != "ADMIN")
        {
            Response.Redirect("~/Login.aspx");
        }
        try
        {
            if (!IsPostBack)
            {
                BindDealTransferTo();
                BindDealTransferFrom();
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    private void BindDealTransferFrom()
    {
        ddl_ptype_From.AppendDataBoundItems = true;
        ddl_ptype_From.Items.Clear();
        SqlCommand cmd = new SqlCommand("UtilityDealFrom", con);
        cmd.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter sda = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        sda.Fill(dt);
        ddl_ptype_From.DataSource = dt;
        ddl_ptype_From.DataTextField = "Group_Type";
        ddl_ptype_From.DataValueField = "Group_Type";
        ddl_ptype_From.Items.Insert(0, new ListItem("-- Select Type --", "0"));
        ddl_ptype_From.DataBind();
    }

    private void BindDealTransferTo()
    {
        try
        {
            ddl_ptype_To.Items.Clear();

            ddl_ptype_To.DataSource = STDom.GetAllGroupType().Tables[0];
            ddl_ptype_To.DataTextField = "GroupType";
            ddl_ptype_To.DataValueField = "GroupType";
            ddl_ptype_To.DataBind();
            ddl_ptype_To.Items.Insert(0, new ListItem("-- Select Type --", ""));
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    protected void Submit_Click(object sender, EventArgs e)
    {
        try
        {
            SqlCommand cmd = new SqlCommand("Insert_From_TO_Utility_DealTransfer", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@From", ddl_ptype_From.SelectedValue);
            cmd.Parameters.AddWithValue("@TO", ddl_ptype_To.SelectedValue);
            cmd.Parameters.AddWithValue("@UserID", Session["UID"]);
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            int k = cmd.ExecuteNonQuery();
            if (k != 0)
            {
                lblmsg.Text = "Deal Inserted Succesfully";
                lblmsg.ForeColor = System.Drawing.Color.CornflowerBlue;
            }
            con.Close();
        }
        catch (Exception ex)
        {
            lblmsg.Text = "Something went Wrong!!Try Aagin";
            lblmsg.ForeColor = System.Drawing.Color.CornflowerBlue;
        }
    }
}