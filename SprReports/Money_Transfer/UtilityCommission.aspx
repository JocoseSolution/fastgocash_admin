﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="UtilityCommission.aspx.cs" Inherits="SprReports_Money_Transfer_UtilityCommission" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        th, td {
            text-align: center !important;
        }

        .deletebutton {
            display: initial !important;
            padding: 0px 10px !important;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="page-wrapperss">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Utility > Commission Master</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Agent Type</label>
                                    <asp:DropDownList ID="ddlGroupType" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlGroupType_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Service</label>
                                    <asp:DropDownList ID="ddlUtilityService" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlUtilityService_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Operator</label>
                                    <asp:DropDownList ID="ddlOperator" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="all">ALL</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Charge Type</label>
                                    <asp:DropDownList CssClass="form-control" ID="ddlChargeType" runat="server">                                        
                                        <asp:ListItem Value="Percentage">Percentage</asp:ListItem>
                                        <asp:ListItem Value="Fixed">Fixed</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Min. Amount (₹)</label>
                                    <asp:TextBox runat="server" ID="txtMinAmount" CssClass="form-control" Text="0.0" onkeypress="return keyRestrict(event,'0123456789');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Max. Amount (₹)</label>
                                    <asp:TextBox runat="server" ID="txtMaxAmount" CssClass="form-control" Text="0.0" onkeypress="return keyRestrict(event,'0123456789');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Charges (₹)</label>
                                    <asp:TextBox runat="server" ID="txtCharges" CssClass="form-control" onkeypress="return NumericWithOneDotOnly(this);"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <br />
                                    <asp:Button ID="btnSubmit" CssClass="button buttonBlue form-control" runat="server" Text="ADD" OnClick="btnSubmit_Click" OnClientClick="return CheckSubmitBtn();" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <div class="row table-responsive text-nowrap">
                                    <asp:GridView ID="utility_grid" runat="server" AutoGenerateColumns="False" Width="100%"
                                        PageSize="20" OnRowCancelingEdit="utility_grid_RowCancelingEdit"
                                        OnRowEditing="utility_grid_RowEditing" OnRowUpdating="utility_grid_RowUpdating" OnRowDeleting="utility_grid_RowDeleting" OnRowDataBound="utility_grid_RowDataBound" AllowPaging="true"
                                        OnPageIndexChanging="utility_grid_PageIndexChanging">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Group Type">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblId" runat="server" Visible="false" Text='<%#Eval("Id") %>'></asp:Label>
                                                    <asp:Label ID="lblGroup_Type" runat="server" Text='<%# !string.IsNullOrEmpty(Eval("Group_Type").ToString()) ? (Eval("Group_Type").ToString()=="all"?"ALL":Eval("Group_Type").ToString()) : "- - - - - - - - -" %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Service">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblService" runat="server" Text='<%# !string.IsNullOrEmpty(Eval("Service").ToString()) ? (Eval("Service").ToString()=="all"?"ALL":Eval("Service").ToString()) : "- - - - - - - - -" %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Operator">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblOperator" runat="server" Text='<%# !string.IsNullOrEmpty(Eval("OperatorName").ToString()) ? Eval("OperatorName").ToString() : "- - - - - - - - -" %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Charges_Type">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCharges_Type" runat="server" Text='<%#Eval("Charges_Type") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Min. Amount (₹)">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMinAmount" runat="server" Text='<%#Eval("MinAmount") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtModifyMinAmount" Style="height: 30px; margin-left: 10px; width: 100px; background: #af3e3ab8; color: #fff; border: 2px solid #000;" Text='<%#Eval("MinAmount") %>' CssClass="form-control" onkeypress="return keyRestrict(event,'0123456789');"></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Max. Amount (₹)">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMaxAmount" runat="server" Text='<%#Eval("MaxAmount") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtModifyMaxAmount" Style="height: 30px; margin-left: 10px; width: 100px; background: #af3e3ab8; color: #fff; border: 2px solid #000;" Text='<%#Eval("MaxAmount") %>' CssClass="form-control" onkeypress="return keyRestrict(event,'0123456789');"></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Charges (₹)">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCharges" runat="server" Text='<%#Eval("Charges") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtModifyCharges" Style="height: 30px; margin-left: 10px; width: 100px; background: #af3e3ab8; color: #fff; border: 2px solid #000;" Text='<%#Eval("Charges") %>' CssClass="form-control" onkeypress="return NumericWithOneDotOnly(this);"></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCreated_Date" runat="server" Text='<%#Eval("CreatedDate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Edit">
                                                <ItemTemplate>
                                                    <asp:Button ID="lnledit" runat="server" Text="Edit" CommandName="Edit" Font-Bold="true" CssClass="newbutton_2" />
                                                    <%--<asp:LinkButton ID="lbtnDelete" runat="server" CommandName="delete" CssClass="newbutton_2" Text="Delete" CommandArgument='<%#Eval("Id")%>' OnClientClick="return confirmDelete();"></asp:LinkButton>--%>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:Button ID="lnlupdate" runat="server" Text="Update" CommandName="Update" Font-Bold="true" CssClass="newbutton_2" />
                                                    <asp:Button ID="lnlcancel" runat="server" Text="Cancel" CommandName="Cancel" Font-Bold="true" CssClass="newbutton_2" />
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Delete">
                                                <ItemTemplate>
                                                    <asp:Button ID="btn_delete" CssClass="newbutton_2" runat="server" Text="Delete" CommandName="Delete" Font-Bold="true" OnClientClick="return confirmDelete();" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <p style="padding: 5px; color: red; font-weight: bold;">NO COMMISSION AVAILABLE, PLEASE ADD NEW COMMISSION !</p>
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function getKeyCode(e) {
            if (window.event)
                return window.event.keyCode;
            else if (e)
                return e.which;
            else
                return null;
        }
        function keyRestrict(e, validchars) {
            var key = '', keychar = '';
            key = getKeyCode(e);
            if (key == null) return true;
            keychar = String.fromCharCode(key);
            keychar = keychar.toLowerCase();
            validchars = validchars.toLowerCase();
            if (validchars.indexOf(keychar) != -1)
                return true;
            if (key == null || key == 0 || key == 8 || key == 9 || key == 13 || key == 27)
                return true;
            return false;
        }
        function NumericWithOneDotOnly(element) {
            var keyCodeEntered = (event.which) ? event.which : (window.event.keyCode) ? window.event.keyCode : -1;
            if ((keyCodeEntered >= 48) && (keyCodeEntered <= 57)) {
                return true;
            }
            else if (keyCodeEntered == 46) {
                if ((element.value) && (element.value.indexOf('.') >= 0))
                    return false;
                else
                    return true;
            }
            return false;
        }
        function confirmDelete() {
            if (confirm("Are you sure want to delete this commission ?")) {
                return true;
            }
            return false;
        }
        function CheckSubmitBtn() {
            if ($("#<%=txtMinAmount.ClientID%>").val().trim() == "") {
                alert("Please enter min. amount !");
                $("#<%=txtMinAmount.ClientID%>").focus();
                return false;
            }
            if ($("#<%=txtMaxAmount.ClientID%>").val().trim() == "") {
                alert("Please enter max. amount !");
                  $("#<%=txtMaxAmount.ClientID%>").focus();
                return false;
            }
            if ($("#<%=txtCharges.ClientID%>").val().trim() == "") {
                alert("Please enter charges !");
                  $("#<%=txtCharges.ClientID%>").focus();
                return false;
            }
            $("#<%=btnSubmit.ClientID%>").val("Please Wait...");
            return true;
        }
    </script>
</asp:Content>

