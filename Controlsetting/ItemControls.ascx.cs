﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControl_ItemControls : System.Web.UI.UserControl
{
    int ID, Width, Height, DisplayTime;
    string Name = "", STRResult = "";
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        try
        {
            ControlItems ObjCI = new ControlItems();
            //ID = Convert.ToInt32(txt_id.Text.Trim().ToString());
            Width = Convert.ToInt32(txt_width.Text.Trim().ToString());
            Height = Convert.ToInt32(txt_height.Text.Trim().ToString());
            DisplayTime = Convert.ToInt32(txt_displaytime.Text.Trim().ToString());
            Name = txt_name.Text.Trim().ToString();
            STRResult = ObjCI.InsertControlDetails(Name, Width, Height, DisplayTime, Session["UID"].ToString(), "InsertControl");
            ClearInputs(Page.Controls);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('" + STRResult + "');", true);
        }
        catch (Exception)
        {
        }
    }
    void ClearInputs(ControlCollection ctrls)
    {
        try
        {
            foreach (Control ctrl in ctrls)
            {
                if (ctrl is TextBox)
                    ((TextBox)ctrl).Text = string.Empty;
                ClearInputs(ctrl.Controls);
            }
        }
        catch (Exception ex)
        {
        }
    }
}